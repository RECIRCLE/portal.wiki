## Problem: Git asks for credentials at every operation.

There are two solutions:
1. Configure an access ssh key (bit difficult and depends on the remote server)
2. Configure git credential cache (simple)  

> Modify your local git config with the following command:  
>  
> ```git config credential.helper 'cache --timeout=21600'```  
>
> This will tell git to remember your credentials for 6 hours.

## Problem: Store executable file in a git repo.  

With the following script git will remember, that the file has to be handled as executable, after a pull the file will have the required permissions.
```bash
 # Stage the script.sh file
 git add script.sh 
 # Flag it as executable
 git update-index --chmod=+x script.sh
 # Commit the change
 git commit -m "Make script.sh executable."
 # Push the commit
 git push
```
