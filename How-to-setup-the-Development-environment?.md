## Devenv

### Easy setup

Use [this](https://git.ik.bme.hu/RECIRCLE/configuration/blob/master/README.md) tutorial to create a devenv the lazy way.

### Setup from scratch

1. Use an Ubuntu 18.04 machine
2. Install python 3.6
3. Install pip for python 3.6
4. Install pipenv
5. Clone the desired repositories