# Instance
https://git.ik.bme.hu/circle/cloud/tree/master/circle/vm/models

Instance fields in CIRCLE:
```python
class BaseResourceConfigModel(Model):

    """Abstract base for models with base resource configuration parameters.
    """
    num_cores = IntegerField(verbose_name=_('number of cores'),
                             help_text=_('Number of virtual CPU cores '
                                         'available to the virtual machine.'),
                             validators=[MinValueValidator(0)])
    ram_size = IntegerField(verbose_name=_('RAM size'),
                            help_text=_('Mebibytes of memory.'),
                            validators=[MinValueValidator(0)])
    max_ram_size = IntegerField(verbose_name=_('maximal RAM size'),
                                help_text=_('Upper memory size limit '
                                            'for balloning.'),
                                validators=[MinValueValidator(0)])
    arch = CharField(max_length=10, verbose_name=_('architecture'),
                     choices=ARCHITECTURES)
    priority = IntegerField(verbose_name=_('priority'),
                            help_text=_('CPU priority.'),
                            validators=[MinValueValidator(0)])
```

```python
class VirtualMachineDescModel(BaseResourceConfigModel):

    """Abstract base for virtual machine describing models.
    """
    access_method = CharField(max_length=10, choices=ACCESS_METHODS,
                              verbose_name=_('access method'),
                              help_text=_('Primary remote access method.'))
    boot_menu = BooleanField(verbose_name=_('boot menu'), default=False,
                             help_text=_(
                                 'Show boot device selection menu on boot.'))
    lease = ForeignKey(Lease, help_text=_("Preferred expiration periods."),
                       verbose_name=_("Lease"))
    raw_data = TextField(verbose_name=_('raw_data'), blank=True, help_text=_(
        'Additional libvirt domain parameters in XML format.'))
    req_traits = ManyToManyField(Trait, blank=True,
                                 help_text=_("A set of traits required for a "
                                             "node to declare to be suitable "
                                             "for hosting the VM."),
                                 verbose_name=_("required traits"))
    system = TextField(verbose_name=_('operating system'),
                       help_text=(_('Name of operating system in format like '
                                    '"Ubuntu 12.04 LTS Desktop amd64".')))
    tags = TaggableManager(blank=True, verbose_name=_("tags"))
    has_agent = BooleanField(verbose_name=_('has agent'), default=True,
                             help_text=_(
                                 'If the machine has agent installed, and '
                                 'the manager should wait for its start.'))
```

```python
class Instance(AclBase, VirtualMachineDescModel, StatusModel, OperatedMixin,
               TimeStampedModel):

    """Virtual machine instance.
    """
    ACL_LEVELS = (
        ('user', _('user')),          # see all details
        ('operator', _('operator')),  # console, networking, change state
        ('owner', _('owner')),        # superuser, can delete, delegate perms
    )
    STATUS = Choices(
        ('NOSTATE', _('no state')),
        ('RUNNING', _('running')),
        ('STOPPED', _('stopped')),
        ('SUSPENDED', _('suspended')),
        ('ERROR', _('error')),
        ('PENDING', _('pending')),
        ('DESTROYED', _('destroyed')),
    )
    name = CharField(blank=True, max_length=100, verbose_name=_('name'),
                     help_text=_("Human readable name of instance."))
    description = TextField(blank=True, verbose_name=_('description'))
    template = ForeignKey(InstanceTemplate, blank=True, null=True,
                          related_name='instance_set', on_delete=SET_NULL,
                          help_text=_("Template the instance derives from."),
                          verbose_name=_('template'))
    pw = CharField(help_text=_("Original password of the instance."),
                   max_length=20, verbose_name=_('password'))
    time_of_suspend = DateTimeField(blank=True, default=None, null=True,
                                    verbose_name=_('time of suspend'),
                                    help_text=_("Proposed time of automatic "
                                                "suspension."))
    time_of_delete = DateTimeField(blank=True, default=None, null=True,
                                   verbose_name=_('time of delete'),
                                   help_text=_("Proposed time of automatic "
                                               "deletion."))
    node = ForeignKey(Node, blank=True, null=True,
                      related_name='instance_set',
                      help_text=_("Current hypervisor of this instance."),
                      verbose_name=_('host node'))
    disks = ManyToManyField('storage.Disk', related_name='instance_set',
                            help_text=_("Set of mounted disks."),
                            verbose_name=_('disks'))
    vnc_port = IntegerField(blank=True, default=None, null=True,
                            help_text=_("TCP port where VNC console listens."),
                            unique=True, verbose_name=_('vnc_port'))
    is_base = BooleanField(default=False)
    owner = ForeignKey(User)
    destroyed_at = DateTimeField(blank=True, null=True,
                                 help_text=_("The virtual machine's time of "
                                             "destruction."))
    objects = Manager()
    active = QueryManager(destroyed_at=None)
```

### Resource related OpS

- arch (choices) - CPU architecture
- num_cores (int) - Number of virtual CPU cores available to the virtual machine.
- priority (int) - CPU priority.
- ram_size (int) - MBs of memory.
- max_ram_size (int) - Upper memory size limit for balloning.

### External properties 

- access_method (text) - Primary remote access method. DB
- boot_menu (bool) - Show boot device selection menu on boot. - 
- lease (Lease) - Preferred expiration periods. DB
- raw_data (text) - Additional libvirt domain parameters in XML format. -
- req_traits (list of Traits) -  A set of traits required for a node to declare to be suitable for hosting the VM. - 
- system (text) - Name of operating system in format like Ubuntu 12.04 LTS Desktop amd64. Gergő
- tags - tags - 
- has_agent (bool) - If the machine has agent installed, and the manager should wait for its start. - 

### VM properties

- name (text) - Human readable name of instance. DB
- description (text) - Description DB
- template (Template) - Template the instance derives from. DB
- pw (text) - Original password of the instance. DB
- time_of_suspend (DateTime) - Proposed time of automatic suspension. DB
- time_of_delete (DateTime) - Proposed time of automatic deletion. DB
- node (Node) - Current hypervisor of this instance. - 
- disks (list of Disks) - Set of mounted disks. DB + OPs
- vnc_port (int) - TCP port where VNC console listens. Ops
- is_base (bool) - Is this machine a base of a Template. - 
- owner (User) - owner DB
- destroyed_at (DateTime) - The virtual machine's time of destruction. - 
- objects - Provides database query operations. - 
- active - ? - 
- interfaces - Network interfaces. OPS
- addresses (ipv4, ipv6, MAC) - Addresses of the vm. OPS
- status - The status of the vm. OPS
- block_device_mapping - ? kell-e
- launched_at - Launch time. OPS
- terminated_at - Time of last terminate. OPS

# Template
https://git.ik.bme.hu/circle/cloud/tree/master/circle/vm/models

```python
class InstanceTemplate(AclBase, VirtualMachineDescModel, TimeStampedModel):

    """Virtual machine template.
    """
    ACL_LEVELS = (
        ('user', _('user')),          # see all details
        ('operator', _('operator')),
        ('owner', _('owner')),        # superuser, can delete, delegate perms
    )
    name = CharField(max_length=100, verbose_name=_('name'),
                     help_text=_('Human readable name of template.'))
    description = TextField(verbose_name=_('description'), blank=True)
    parent = ForeignKey('self', null=True, blank=True,
                        verbose_name=_('parent template'),
                        on_delete=SET_NULL,
                        help_text=_('Template which this one is derived of.'))
    disks = ManyToManyField('storage.Disk', verbose_name=_('disks'),
                            related_name='template_set',
                            help_text=_('Disks which are to be mounted.'))
    owner = ForeignKey(User)
```

- name (CharField): Human readable name of template. DB
- description (TextField): Description of the template. DB
- parent (Template): Template which this one is derived of. Ops
- disks (list of Disks): Disks which are to be mounted. Ops   
- owner (User): Owner. DB


# Image
https://git.ik.bme.hu/circle/cloud/blob/master/circle/storage/models.py

```python

class Disk(TimeStampedModel):

    """A virtual disk.
    """
    TYPES = [('qcow2-norm', 'qcow2 normal'), ('qcow2-snap', 'qcow2 snapshot'),
             ('iso', 'iso'), ('raw-ro', 'raw read-only'), ('raw-rw', 'raw')]
    BUS_TYPES = (('virtio', 'virtio'), ('ide', 'ide'), ('scsi', 'scsi'))
    name = CharField(blank=True, max_length=100, verbose_name=_("name"))
    filename = CharField(max_length=256, unique=True,
                         verbose_name=_("filename"))
    datastore = ForeignKey(DataStore, verbose_name=_("datastore"),
                           help_text=_("The datastore that holds the disk."))
    type = CharField(max_length=10, choices=TYPES)
    bus = CharField(max_length=10, choices=BUS_TYPES, null=True, blank=True,
                    default=None)
    size = FileSizeField(null=True, default=None)
    base = ForeignKey('self', blank=True, null=True,
                      related_name='derivatives')
    dev_num = CharField(default='a', max_length=1,
                        verbose_name=_("device number"))
    destroyed = DateTimeField(blank=True, default=None, null=True)

    is_ready = BooleanField(default=False)
```

- name (CharField): Name of the disk. DB
- filename (CharField): Filename. -
- datastore (DataStore): The datastore that holds the disk. -
- type (CharField): Type of the disk. Ops
- bus (CharField): The bus type. -
- size (FileSizeField): The size of the disk. Ops
- base (Disk): Base disk. -
- dev_num (CharField): Device number. -
- destroyed (DateTimeField): When was the disk destroyed? -
- is_ready (BooleanField): Is the disk ready? -


# Storage
https://git.ik.bme.hu/circle/cloud/blob/master/circle/storage/models.py

```python
class DataStore(Model):

    """Collection of virtual disks.
    """
    name = CharField(max_length=100, unique=True, verbose_name=_('name'))
    path = CharField(max_length=200, unique=True, verbose_name=_('path'))
    hostname = CharField(max_length=40, unique=True,
                         verbose_name=_('hostname'))
```

- name (CharField): Name. DB
- vm (Instance): the instance this storage is connected to + DB OPs
- path (CharField): Path -
- hostname (CharField): Hostname. - 