# Installing notes

## Prerequisites

- OpenStack deployment somewhere
- Postgresql
- Redis

## Reqirements

- install python requirements from Pipfile.lock

## Configurations

- OpenStack authentication credentials
- Postgre, Redis password, port, etc.
- Background task running times

## Starting when server running

- Background tasks with celery