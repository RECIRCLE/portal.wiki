Here are our guideline for using git in the RECIRCLE project.

### Branches

**Master**: This branch represent the production state of the software. Never commit to this branch! We will merge commits here if we release a new version of RECIRCLE.

**DEV**: This is a development branch where all the new features gets merged. Never commit directly to this branch! We use merge requests to update this branch. 

**feature-***: All the new features has to be developed on a separate branch. That way it is easy to revert them if a new feature is not needed. These branches will be merged to the DEV branch.

**bugfix-***: Bug fixes are developed in separate branches similarly to features, but they are allowed to be merged to master, if they are urgent.

**...-***: Other prefixes are allowed if they the above two are not sufficient. Please update this wiki if needed.

### Git workflow

New feature

1. Clone or pull the latest version of DEV.
2. Create a new branch with an available prefix:  ```git checkout -b feature-new-feature```
3. Code and commit to that branch.
4. When your changes are stable and you wrote the tests open a new merge request on Gitlab.
5. Assign it someone, who you think can review your code.

Code review

1. Someone assigned a merge request to you.
2. Check the code and test the new feature locally if needed.
3. Pose questions and advices.
4. If it's complete merge it.

Release

*Under construction*

### Submodules

For the [portal](https://git.ik.bme.hu/RECIRCLE/portal) repo we use [interface-openstack](https://git.ik.bme.hu/RECIRCLE/interface-openstack) as a submodule.

This is a distinct functionality of git and requires special knowledge.

Cloning the repo

1. Clone the portal repo as usual.
2. Run ```git submodule init``` to initialize the local configuration file.
3. Run ```git submodule update``` to fetch all the data for the submodule.

Alternatively you can use the ```git clone``` command with the ```--recurse-submodules``` parameter to init and update the repo automatically.

Changes in the submodule

Making changes in the submodule, usually doesn't reflect on your local copy.

Run ```git submodule update --remote``` to acquire the latest version. Alternatively you can navigate to the submodule folder and check out the latest version of any branch. Although this is an ill-advised practice. If you want changes in the interface repository clone it separately.



