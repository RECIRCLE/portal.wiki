# RECIRCLE Wiki

This is the home page of the RECIRCLE Wiki.

The documentation structure is under construction.

[Mapping of old CIRCLE Django models to RECIRCLE models](New models)

## Developer guidelines

[Git](Developer guideline   git)

## FAQ

[How to setup the development environment?](https://git.ik.bme.hu/RECIRCLE/portal/wikis/How-to-setup-the-Development-environment%3F)

## Tips and tricks

- [Git](Git tips and tricks)

